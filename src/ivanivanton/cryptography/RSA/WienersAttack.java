package ivanivanton.cryptography.RSA;

import ivanivanton.cryptography.Key;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class WienersAttack {

    public static Long getPrivateExponent(Key RSAPublicKey) {
        long publicExp = RSAPublicKey.numbers.get(0);
        long publicN = RSAPublicKey.numbers.get(1);

        List<Long> continuedFractionCoefficients =
                WienersAttack.getContinuedFractionCoefficients(
                        BigInteger.valueOf(publicExp), BigInteger.valueOf(publicN));

        List<List<Long>> matchingFractions = WienersAttack.getMatchingFractions(continuedFractionCoefficients);

        for (List<Long> fraction :
                matchingFractions) {
            double possibleValue = ((double)publicExp * fraction.get(1) - 1) / (double)fraction.get(0);
            if(!(Math.rint(possibleValue) == possibleValue)) continue;

            long possibleValueLong = (long) possibleValue;
            List<String> roots =
                    WienersAttack.calculateRoots(1, -(publicN - possibleValueLong + 1), publicN);
            if(roots.isEmpty()) continue;

            Pattern intValue = Pattern.compile("\\d+\\.0+");
            if(!intValue.matcher(roots.get(0)).matches() || !intValue.matcher(roots.get(1)).matches()) continue;

            long firstRootL = Long.parseLong(roots.get(0).split("\\.")[0]);
            long secondRootL = Long.parseLong(roots.get(1).split("\\.")[0]);

            if(firstRootL * secondRootL == publicN) {
                return fraction.get(1);
            }
        }

        return -1L;
    }

    private static List<Long> getContinuedFractionCoefficients(BigInteger numerator, BigInteger denominator) {
        BigInteger temp;
        ArrayList<Long> coefficients = new ArrayList<>();

        while (!denominator.equals(BigInteger.ZERO)) {
            temp = numerator.mod(denominator);
            coefficients.add(numerator.subtract(temp).divide(denominator).longValue());
            numerator = denominator;
            denominator = temp;
        }

        return coefficients;
    }

    private static List<List<Long>> getMatchingFractions(List<Long> continuedFractionCoefficients) {
        List<List<Long>> matchingFractions = new LinkedList<>();
        matchingFractions.add(new ArrayList<>() {
            {
                add(1L);
                add(0L);
            }
        });
        matchingFractions.add(new ArrayList<>() {
            {
                add(continuedFractionCoefficients.get(0));
                add(1L);
            }
        });

        for (int i = 2; i < continuedFractionCoefficients.size(); i++) {
            int finalI = i;
            matchingFractions.add(new ArrayList<>() {
                {
                    add(continuedFractionCoefficients.get(finalI - 1) * matchingFractions.get(finalI - 1).get(0)
                            + matchingFractions.get(finalI - 2).get(0));
                    add(continuedFractionCoefficients.get(finalI - 1) * matchingFractions.get(finalI - 1).get(1)
                            + matchingFractions.get(finalI - 2).get(1));
                }
            });
        }

        matchingFractions.remove(0);
        matchingFractions.remove(0);
        matchingFractions.remove(0);
        return matchingFractions;
    }

    private static List<String> calculateRoots(long a, long b, long c) {
        List<String> roots = new ArrayList<>();
        if (a == 0) {
            return roots;
        }
        long discriminant = b * b - 4 * a * c;
        double discriminantSqrt = sqrt(abs(discriminant));
        if (discriminant > 0) {
            roots.add(String.valueOf((-b + discriminantSqrt) / (2 * a)));
            roots.add(String.valueOf((-b - discriminantSqrt) / (2 * a)));
        } else if (discriminant == 0) {
            roots.add(String.valueOf(-(double) b / (2 * a)));
            roots.add(String.valueOf(-(double) b / (2 * a)));
        }
        else {
            roots.add(-(double) b / (2 * a) + " + i" + discriminantSqrt);
            roots.add(-(double) b / (2 * a) + " - i" + discriminantSqrt);
        }

        return roots;
    }
}
