package ivanivanton.cryptography.RSA;

import ivanivanton.cryptography.Key;
import ivanivanton.cryptography.KeyPair;

import java.math.BigInteger;
import java.util.List;

public class WienersAttackExample {
    public static void main(String[] args) throws Exception {
        RSA rsa = new RSA();


        Key publicKey = new Key(28797887L, 34872451L);
        Key privateKey = new Key(23L, 34872451L);
        KeyPair keys = new KeyPair(publicKey, privateKey);

        String text = "Hello world!";
        System.out.println("Text: " +text);
        System.out.println("Keys:" +keys);
        String ciphered = "";
        try {
            ciphered = rsa.cipher(text, keys);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Ciphered: " +ciphered);
        keys = null;

        long privateExp = WienersAttack.getPrivateExponent(publicKey);
        if(privateExp != -1) {
            privateKey = new Key(privateExp, 34872451L);
            keys = new KeyPair(publicKey, privateKey);
            System.out.println("Attacked private key:" + privateKey);

            System.out.println("Attacked text:" + rsa.decipher(ciphered, keys));
        } else {
            System.out.println("Can't attack this private key");
        }
    }
}
