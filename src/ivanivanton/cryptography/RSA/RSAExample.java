package ivanivanton.cryptography.RSA;

import ivanivanton.cryptography.KeyPair;

public class RSAExample {
    public static void main(String[] args) throws Exception {
        RSA rsa = new RSA();
        KeyPair keys = rsa.generateKeyPair();
        System.out.println(keys);
        String ciphered = rsa.cipher("Функции абзацного членения различаются в \n" +
                "зависимости от того, диалогическая перед нами или монологическая речь. Абзац в диалогической речи\n" +
                " выполняет формальную функцию, так как разграничивает реплики разных людей. В монологической речи\n " +
                "абзац выполняет логико-смысловую, акцентную, экспрессивно-эмоциональную функции.\n", keys);
        System.out.println(ciphered);
        String deciphered = rsa.decipher(ciphered, keys);
        System.out.println(deciphered);
    }
}
