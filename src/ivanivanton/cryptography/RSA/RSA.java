package ivanivanton.cryptography.RSA;

import java.math.BigInteger;
import java.util.HashMap;

import ivanivanton.cryptography.Cipher;
import ivanivanton.cryptography.KeyPair;
import ivanivanton.cryptography.Utils;
import ivanivanton.cryptography.Key;

public class RSA extends Cipher {

    public KeyPair generateKeyPair() {
        int[] pairOfPrimes;
        int firstPrime;
        int secondPrime;

        long n = 1;
        int EulerNumber = 1;

        long openExp = 1;

        int[] abxy;
        long closedExp = 1;
        while (closedExp == EulerNumber || closedExp == openExp) {
            pairOfPrimes = Utils.getPairOfPrimes(primes);
            firstPrime = pairOfPrimes[0];
            secondPrime = pairOfPrimes[1];

            n = (long) firstPrime * secondPrime;
            EulerNumber = (firstPrime - 1) * (secondPrime - 1);

            openExp = Utils.getRandomCoprime(EulerNumber);

            long[] abxy1 = {openExp, EulerNumber ,1 ,1};
            Utils.gcdExtended(abxy1);
            closedExp = (abxy1[2] % EulerNumber + EulerNumber) % EulerNumber;
        }

        Key publicKey = new Key(openExp, n);
        Key privateKey = new Key(closedExp, n);

        return new KeyPair(publicKey, privateKey);
    }

    public String cipher(String text, KeyPair keyPair) throws Exception {
        if(keyPair == null || keyPair.isEmpty()
                || keyPair.privateKey().numbers.size() != 2 || keyPair.publicKey().numbers.size() != 2) {
            throw new Exception("Can't cipher without key pair");
        }

        StringBuilder result = new StringBuilder();
        char[] textChars = text.toCharArray();
        HashMap<Character, Integer> charCodes = Utils.getCharCodes();
        Integer number;

        BigInteger publicExpBig = BigInteger.valueOf(keyPair.publicKey().numbers.get(0));
        BigInteger nBig = BigInteger.valueOf(keyPair.publicKey().numbers.get(1));


        for (char textChar : textChars) {
            number = charCodes.get(textChar);
            BigInteger numberBig = new BigInteger(number.toString());
            BigInteger product = numberBig.modPow(publicExpBig, nBig);

            String block = Long.toString(product.longValue(), 32);
            block = switch (block.length()) {
                case 1 -> "00000" + block;
                case 2 -> "0000" + block;
                case 3 -> "000" + block;
                case 4 -> "00" + block;
                case 5 -> "0" + block;
                default -> Long.toString(product.longValue(), 32);
            };
            result.append(block);
        }
        return result.toString();
    }

    public String decipher(String text, KeyPair keyPair) throws Exception {
        if(keyPair == null || keyPair.isEmpty()
                || keyPair.privateKey().numbers.size() != 2 || keyPair.publicKey().numbers.size() != 2) {
            throw new Exception("Can't decipher without key pair");
        }
        StringBuilder encoded = new StringBuilder();

        String[] numbers = text.split("(?<=\\G......)");
        for (String number:
             numbers) {
            BigInteger numberBig =  BigInteger.valueOf(Long.parseLong(number, 32));
            BigInteger privateExpBig = BigInteger.valueOf(keyPair.privateKey().numbers.get(0));
            BigInteger nBig = BigInteger.valueOf(keyPair.privateKey().numbers.get(1));

            BigInteger product = numberBig.modPow(privateExpBig, nBig);
            encoded.append(product);
        }

        return decodeString(encoded.toString());
    }
}
