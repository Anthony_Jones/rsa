package ivanivanton.cryptography;


public record KeyPair(Key publicKey, Key privateKey) {

    @Override
    public String toString() {
        return "KeyPair{" +
                "publicKey=" + publicKey +
                ", privateKey=" + privateKey +
                '}';
    }

    public boolean isEmpty() {
        return publicKey == null || privateKey == null;
    }
}
