package ivanivanton.cryptography;

import java.util.HashMap;
import java.util.List;

public abstract class Cipher {

    protected List<Integer> primes;

    public Cipher() {
        primes = Utils.sieveOfEratosthenes(2000);
    }

    public abstract KeyPair generateKeyPair();
    public abstract String cipher(String text, KeyPair keyPair) throws Exception;
    public abstract String decipher(String text, KeyPair keyPair) throws Exception;
    
    public String decodeString(String encoded) {
        HashMap<Character, Integer> charsCodes = Utils.getCharCodes();

        String[] chars = encoded.split("(?<=\\G...)");
        String result = String.join("|", chars);

        for(HashMap.Entry<Character, Integer> entry : charsCodes.entrySet()){
            result = result.replaceAll(entry.getValue().toString(), entry.getKey().toString());
        }
        result = result.replaceAll("\\|", "");

        return result;
    }
}
