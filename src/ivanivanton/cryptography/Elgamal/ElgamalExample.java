package ivanivanton.cryptography.Elgamal;

import ivanivanton.cryptography.KeyPair;
import ivanivanton.cryptography.Utils;

public class ElgamalExample {
    public static void main(String[] args) throws Exception {
        Elgamal elgamal = new Elgamal();
        KeyPair keys = elgamal.generateKeyPair();

        System.out.println(keys);

        String text = "Hello world!";
        String ciphered = elgamal.cipher(text, keys);
        System.out.println(ciphered);

        System.out.println(elgamal.decipher(ciphered, keys));

    }
}
