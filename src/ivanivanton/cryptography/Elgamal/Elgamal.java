package ivanivanton.cryptography.Elgamal;

import ivanivanton.cryptography.Cipher;
import ivanivanton.cryptography.Key;
import ivanivanton.cryptography.KeyPair;
import ivanivanton.cryptography.Utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Elgamal extends Cipher {
    public Elgamal() {
        primes = Utils.sieveOfEratosthenes(4000);
    }

    @Override
    public KeyPair generateKeyPair() {
        int min;
        int max;
        int firstPrimeIndex;
        long firstPrime;


        ArrayList<Integer> primitiveRootsOfP;
        long firstPrimePrimitiveRoot;

        long secondPrime;
        Key privateKey = new Key(1L);

        BigInteger bigPrimitiveRoot;
        BigInteger bigX;
        BigInteger bigP ;
        long y;

        Key publicKey = new Key(1L, 1L, 1L);

        while(Objects.equals(publicKey.numbers.get(0), publicKey.numbers.get(1)) || Objects.equals(publicKey.numbers.get(2), privateKey.numbers.get(0))) {
            min = 168;
            max = primes.size() - 1;
            firstPrimeIndex = Utils.getRandomMinMax(min, max);
            firstPrime = primes.get(firstPrimeIndex);


            primitiveRootsOfP = Utils.getPrimitiveRootsOfPrime(firstPrime);
            max = primitiveRootsOfP.size() - 1;
            firstPrimePrimitiveRoot = primitiveRootsOfP.get(Utils.getRandomMinMax(min, max));

            max = firstPrimeIndex - 1;
            secondPrime = primes.get(Utils.getRandomMinMax(min, max));
            privateKey = new Key(secondPrime);

            bigPrimitiveRoot = BigInteger.valueOf(firstPrimePrimitiveRoot);
            bigX = BigInteger.valueOf(secondPrime);
            bigP = BigInteger.valueOf(firstPrime);
            y = bigPrimitiveRoot.modPow(bigX, bigP).longValueExact();

            publicKey = new Key(y, firstPrimePrimitiveRoot, firstPrime);
        }

        return new KeyPair(publicKey, privateKey);
    }

    @Override
    public String cipher(String text, KeyPair keyPair) throws Exception {
        if(keyPair == null || keyPair.isEmpty()
                || keyPair.privateKey().numbers.size() != 1 || keyPair.publicKey().numbers.size() != 3) {
            throw new Exception("Can't cipher without key pair");
        }

        BigInteger sessionCoprimeWithFirstPrime =
                BigInteger.valueOf(Utils.getRandomCoprime(Math.toIntExact(keyPair.publicKey().numbers.get(2) - 1)));
        BigInteger bigY = BigInteger.valueOf(keyPair.publicKey().numbers.get(0));
        BigInteger bigYPowed = Utils.binpowBig(bigY, sessionCoprimeWithFirstPrime);
        BigInteger bigG = BigInteger.valueOf(keyPair.publicKey().numbers.get(1));
        BigInteger bigP = BigInteger.valueOf(keyPair.publicKey().numbers.get(2));

        StringBuilder result = new StringBuilder();
        char[] textChars = text.toCharArray();
        HashMap<Character, Integer> charCodes = Utils.getCharCodes();
        Integer number;
        BigInteger firstBlock = bigG.modPow(sessionCoprimeWithFirstPrime, bigP);

        for (char textChar : textChars) {
            number = charCodes.get(textChar);
            BigInteger numberBig = BigInteger.valueOf(number);

            BigInteger secondBlock = numberBig.multiply(bigYPowed).mod(bigP);

            String firstBlockString = Long.toString(firstBlock.longValue(), 32);
            if(firstBlockString.length() == 2) {
                firstBlockString = "0" + firstBlockString;
            } else if(firstBlockString.length() == 1) {
                firstBlockString = "00" + firstBlockString;
            }
            result.append(firstBlockString);

            String secondBlockString = Long.toString(secondBlock.longValue(), 32);
            if(secondBlockString.length() == 2) {
                secondBlockString = "0" + secondBlockString;
            } else if(secondBlockString.length() == 1) {
                secondBlockString = "00" + secondBlockString;
            }
            result.append(secondBlockString);
        }
        return result.toString();

    }

    @Override
    public String decipher(String text, KeyPair keyPair) throws Exception {
        if(keyPair == null || keyPair.isEmpty()
                || keyPair.privateKey().numbers.size() != 1 || keyPair.publicKey().numbers.size() != 3) {
            throw new Exception("Can't decipher without key pair");
        }

        String[] numbers = text.split("(?<=\\G...)");

        Long firstPrime = keyPair.publicKey().numbers.get(2);

        BigInteger firstBlock = BigInteger.valueOf(Long.parseLong(numbers[0], 32));
        BigInteger powNumber =
                BigInteger.valueOf(firstPrime - 1 - keyPair.privateKey().numbers.get(0));
        BigInteger firstBlockPowed = Utils.binpowBig(firstBlock, powNumber);

        StringBuilder encoded = new StringBuilder();
        for (int i = 1; i < numbers.length; i += 2) {
            BigInteger secondBlock = BigInteger.valueOf(Long.parseLong(numbers[i], 32));
            BigInteger resultNumber;

            resultNumber = secondBlock.multiply(firstBlockPowed).mod(BigInteger.valueOf(firstPrime));

            encoded.append(resultNumber);
        }

        return decodeString(encoded.toString());
    }
}
