package ivanivanton.cryptography;

import java.math.BigInteger;
import java.util.*;

public class Utils {
    public static int getRandomMinMax(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1) + min);
    }

    public static int[] getPairOfPrimes(List<Integer> primes) {
        int min = 51;
        int max = primes.size() - 1;
        int firstNumber = primes.get(getRandomMinMax(min, max));
        int secondNumber = primes.get(getRandomMinMax(min, max));

        while (Math.abs(firstNumber - secondNumber) < 15) {
            secondNumber = primes.get(getRandomMinMax(min, max));
        }

        int[] primePair = new int[2];
        primePair[0] = firstNumber;
        primePair[1] = secondNumber;
        return primePair;
    }

    public static List<Integer> sieveOfEratosthenes(int n) {
        boolean[] prime = new boolean[n + 1];
        Arrays.fill(prime, true);
        for (int p = 2; p * p <= n; p++) {
            if (prime[p]) {
                for (int i = p * 2; i <= n; i += p) {
                    prime[i] = false;
                }
            }
        }
        List<Integer> primeNumbers = new LinkedList<>();
        for (int i = 2; i <= n; i++) {
            if (prime[i]) {
                primeNumbers.add(i);
            }
        }
        return primeNumbers;
    }

    public static int gcd(int a, int b) {
        while (b > 0) {
            a %= b;
            int temp = a;
            a = b;
            b = temp;
        }
        return a;
    }

    public static long gcdExtended(long[] abxy) {
        if (abxy[0] == 0) {
            abxy[2] = 0;
            abxy[3] = 1;
            return abxy[1];
        }

        long[] temp = {abxy[1] % abxy[0], abxy[0], 1, 1};
        long gcd = gcdExtended(temp);

        abxy[2] = temp[3] - (abxy[1] / abxy[0]) * temp[2];
        abxy[3] = temp[2];
        return gcd;
    }

    public static int getRandomCoprime(int Number) {
        List<Integer> coprimes = new LinkedList<>();
        for (int i = 2; i < Number; i++) {
            if (gcd(i, Number) == 1) {
                coprimes.add(i);
            }
        }

        int min = 0;
        int max = coprimes.size() - 1;
        int result = 0;
        while ((result = coprimes.get(getRandomMinMax(min, max))) <= 3);

        return result;
    }

    public static BigInteger binpowBig(BigInteger a, BigInteger n) {
        BigInteger res = new BigInteger("1");
        while (n.signum() != 0) {
            if ((n.and(BigInteger.ONE)).intValue() == 1) {
                res = res.multiply(a);
            }
            a = a.multiply(a);
            n = n.shiftRight(1);
        }
        return res;
    }



    public static HashMap<Character, Integer> getCharCodes() {
        HashMap<Character, Integer> map = new HashMap<>();
        map.put('a', 111);
        map.put('b', 112);
        map.put('c', 113);
        map.put('d', 114);
        map.put('e', 115);
        map.put('f', 116);
        map.put('g', 117);
        map.put('h', 118);
        map.put('i', 119);
        map.put('j', 120);
        map.put('k', 121);
        map.put('l', 122);
        map.put('m', 123);
        map.put('n', 124);
        map.put('o', 125);
        map.put('p', 126);
        map.put('q', 127);
        map.put('r', 128);
        map.put('s', 129);
        map.put('t', 130);
        map.put('u', 131);
        map.put('v', 132);
        map.put('w', 133);
        map.put('x', 134);
        map.put('y', 135);
        map.put('z', 136);

        map.put('а', 137);
        map.put('б', 138);
        map.put('в', 139);
        map.put('г', 140);
        map.put('д', 141);
        map.put('е', 142);
        map.put('ё', 143);
        map.put('ж', 144);
        map.put('з', 145);
        map.put('и', 146);
        map.put('й', 147);
        map.put('к', 148);
        map.put('л', 149);
        map.put('м', 150);
        map.put('н', 151);
        map.put('о', 152);
        map.put('п', 153);
        map.put('р', 154);
        map.put('с', 155);
        map.put('т', 156);
        map.put('у', 157);
        map.put('ф', 158);
        map.put('х', 159);
        map.put('ц', 160);
        map.put('ч', 161);
        map.put('ш', 162);
        map.put('щ', 163);
        map.put('ъ', 164);
        map.put('ы', 165);
        map.put('ь', 166);
        map.put('э', 167);
        map.put('ю', 168);
        map.put('я', 169);
        map.put(' ', 170);

        map.put('A', 171);
        map.put('B', 172);
        map.put('C', 173);
        map.put('D', 174);
        map.put('E', 175);
        map.put('F', 176);
        map.put('G', 177);
        map.put('H', 178);
        map.put('I', 179);
        map.put('J', 180);
        map.put('K', 181);
        map.put('L', 182);
        map.put('M', 183);
        map.put('N', 184);
        map.put('O', 185);
        map.put('P', 186);
        map.put('Q', 187);
        map.put('R', 188);
        map.put('S', 189);
        map.put('T', 190);
        map.put('U', 191);
        map.put('V', 192);
        map.put('W', 193);
        map.put('X', 194);
        map.put('Y', 195);
        map.put('Z', 196);

        map.put('А', 197);
        map.put('Б', 198);
        map.put('В', 199);
        map.put('Г', 200);
        map.put('Д', 201);
        map.put('Е', 202);
        map.put('Ё', 203);
        map.put('Ж', 204);
        map.put('З', 205);
        map.put('И', 206);
        map.put('Й', 207);
        map.put('К', 208);
        map.put('Л', 209);
        map.put('М', 210);
        map.put('Н', 211);
        map.put('О', 212);
        map.put('П', 213);
        map.put('Р', 214);
        map.put('С', 215);
        map.put('Т', 216);
        map.put('У', 217);
        map.put('Ф', 218);
        map.put('Х', 219);
        map.put('Ц', 221);
        map.put('Ч', 222);
        map.put('Ш', 223);
        map.put('Щ', 224);
        map.put('Ъ', 225);
        map.put('Ы', 226);
        map.put('Ь', 227);
        map.put('Э', 228);
        map.put('Ю', 229);
        map.put('Я', 230);
        map.put(',', 231);
        map.put('!', 232);
        map.put('?', 233);
        map.put('.', 234);
        map.put('-', 235);
        map.put('\n', 236);
        map.put('(', 237);
        map.put(')', 238);
        map.put('–', 239);
        return map;
    }


    public static ArrayList<Integer> getPrimitiveRootsOfPrime(long primeNumber) {
        ArrayList<Integer> result = new ArrayList<>();
        ArrayList<BigInteger> fact = new ArrayList<>();
        BigInteger phi = BigInteger.valueOf(primeNumber);
        long n = phi.longValue() - 1;
        for (long i=2; i*i<=n; ++i) {
            if (n % i == 0) {
                fact.add(BigInteger.valueOf(i));
                while (n % i == 0)
                    n /= i;
            }
        }
        if (n > 1) {
            fact.add(BigInteger.valueOf(n));
        }

        for (int res=2; res<=primeNumber-1; ++res) {
            boolean ok = true;
            for (int i=0; i<fact.size() && ok; ++i) {
                BigInteger resBig = new BigInteger(String.valueOf(res));
                ok &= !resBig.modPow(phi.divide(fact.get(i)), BigInteger.valueOf(primeNumber)).equals(BigInteger.ONE);
            }
            if (ok)  {
                result.add(res);
            };
        }
        return result;
    }
}
