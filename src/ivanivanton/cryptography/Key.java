package ivanivanton.cryptography;

import java.util.ArrayList;
import java.util.List;

public class Key {
    public final ArrayList<Long> numbers;

    public Key(Long ...numbersList) {
        numbers = new ArrayList<>();
        numbers.addAll(List.of(numbersList));
    }

    @Override
    public String toString() {
        return numbers.toString();
    }
}
