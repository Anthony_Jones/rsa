import ivanivanton.cryptography.Key;
import ivanivanton.cryptography.KeyPair;
import ivanivanton.cryptography.RSA.RSA;
import ivanivanton.cryptography.RSA.WienersAttack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WienerAttackTest {

    private RSA rsa;

    private Key publicKey;
    private Key privateKey;
    private KeyPair keys;

    private final String text = "йцукенгшщзхъфывапролджэячсмитьбюё" +
            "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДДЭЖЯЧСМИТЬЮБЁ" +
            "qwertyuiopasdfghjklzxcvbnm" +
            "QWERTYUIOPASDFGHJKLZXCVBNM"+
            " ,.!?-\n";

    @Before
    public void setUpAttack() {
        rsa = new RSA();
        publicKey = new Key(28797887L, 34872451L);
        privateKey = new Key(23L, 34872451L);
        keys = new KeyPair(publicKey, privateKey);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void successAttack() throws Exception {
        String ciphered =  rsa.cipher(text, keys);
        keys = null;

        long privateExp = WienersAttack.getPrivateExponent(publicKey);
        privateKey = new Key(privateExp, 34872451L);
        keys = new KeyPair(publicKey, privateKey);

        Assert.assertEquals(text, rsa.decipher(ciphered, keys));
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void failureAttack() throws Exception {
        for (int i = 0; i < 200; i++) {
            keys = rsa.generateKeyPair();
            String ciphered =  rsa.cipher(text, keys);

            Key publicKey = keys.publicKey();
            long privateExp = WienersAttack.getPrivateExponent(publicKey);
            Key privateKey = new Key(privateExp, publicKey.numbers.get(1));
            keys = new KeyPair(publicKey, privateKey);

            Assert.assertNotEquals(text, rsa.decipher(ciphered, keys));
        }

    }
}
