import ivanivanton.cryptography.Key;
import ivanivanton.cryptography.KeyPair;
import ivanivanton.cryptography.RSA.RSA;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class RSATest {

    private RSA cipher;
    private KeyPair keyPair;
    private final String text = "йцукенгшщзхъфывапролджэячсмитьбюё" +
            "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДДЭЖЯЧСМИТЬЮБЁ" +
            "qwertyuiopasdfghjklzxcvbnm" +
            "QWERTYUIOPASDFGHJKLZXCVBNM" +
            " ,.!?-\n";

    @Before
    public void createRSA() {
        cipher = new RSA();
        keyPair = cipher.generateKeyPair();
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void decipherSuccess() throws Exception {
        for (int i = 0; i < 2000; i++) {
            KeyPair keyPair = cipher.generateKeyPair();
            String ciphered = cipher.cipher(text, keyPair);
            String deciphered = cipher.decipher(ciphered, keyPair);
            Assert.assertEquals(text, deciphered);
            System.out.println(i + ": "+ ciphered);
            System.out.println(keyPair);
            System.out.println();
        }
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void cipherFailureWithEmptyKeyPair() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Can't cipher without key pair");

        KeyPair keyPair = new KeyPair(new Key(), new Key());
        cipher.cipher(text, keyPair);
    }

    @Test
    public void decipherFailureWithEmptyKeyPair() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Can't decipher without key pair");

        KeyPair keyPair = new KeyPair(new Key(), new Key());
        cipher.decipher(text, keyPair);
    }
}
