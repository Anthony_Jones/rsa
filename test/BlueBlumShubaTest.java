import ivanivanton.cryptography.BlueBlumShuba.BlueBlumShuba;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class BlueBlumShubaTest {
    private BlueBlumShuba blueBlumShuba;


    @Before
    public void createBlueBlumShuba()
    {
        blueBlumShuba = new BlueBlumShuba();
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void bit256Time() {
        List<Long> time = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            time.add(System.currentTimeMillis());
            BigInteger number = blueBlumShuba.generateRandomNumber(256);
            time.set(time.size() - 1, System.currentTimeMillis()- time.get(time.size() - 1) );
            System.out.print(i+": ");
            System.out.println(number);
        }
        double avg = time.stream().mapToDouble(d -> d).average().orElse(0.0);
        System.out.println("Avarage gen time:" + avg);
        Assert.assertTrue(avg < 1);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void bit512Time() {
        List<Long> time = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            time.add(System.currentTimeMillis());
            BigInteger number = blueBlumShuba.generateRandomNumber(512);
            time.set(time.size() - 1, System.currentTimeMillis()- time.get(time.size() - 1) );
            System.out.print(i+": ");
            System.out.println(number);
        }
        double avg = time.stream().mapToDouble(d -> d).average().orElse(0.0);
        System.out.println("Avarage gen time:" + avg);
        Assert.assertTrue(avg < 1.5);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void bit1024Time() {
        List<Long> time = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            time.add(System.currentTimeMillis());
            BigInteger number = blueBlumShuba.generateRandomNumber(512);
            time.set(time.size() - 1, System.currentTimeMillis()- time.get(time.size() - 1) );
            System.out.print(i+": ");
            System.out.println(number);
        }
        double avg = time.stream().mapToDouble(d -> d).average().orElse(0.0);
        System.out.println("Avarage gen time:" + avg);
        Assert.assertTrue(avg < 2);
    }

}

